/*eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */

//import services from './services/services';
//import specialists from './services/specialists';
//import servedQueue from './services/served-queue';

import waitingQueue from './services/waiting-queue';

console.log(waitingQueue);

import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'

Vue.config.productionTip = false

new Vue({
    vuetify,
    render: h => h(App),
}).$mount('#app')
