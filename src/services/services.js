class Services {
    constructor() {
        this.list = [];
        this.lastId = 0;
        this.min = 0;
        this.max = 1000;
        this.availableIntervals = [];

        this.updateAvailableIntervals();

        this.add('Main service', 200, 210);
        this.add('Alternate service', 300, 305);

        // [0..200,210,..,300,305..1000]
    }

    add(name, rangeStart, rangeEnd) {
        // todo test if range is available

        this.lastId++;

        // todo - mising range validity check
        // if range is invalid, trow exception

        this.list.push({
            id: this.lastId,
            name: name,
            rangeStart: rangeStart,
            rangeEnd: rangeEnd,
            lastId: null
        });

        this.updateAvailableIntervals();
    }

    get(serviceId) {
        let [service] = this.list.filter(item => item.id === serviceId);
        return service;
    }

    getAll() {
        return this.list;
    }

    getMin() {
        return this.min;
    }

    getMax() {
        return this.max;
    }

    getRange(serviceId) {
        let service = this.get(serviceId);
        return {
            start: service.rangeStart,
            end: service.rangeEnd
        };
    }

    updateAvailableIntervals() {
        if (this.availableIntervals.length == 0) {
            this.availableIntervals = [{ "start": this.min, "end": this.max }];
            return;
        }

        let entries = [this.min, this.max];
        this.list.map(item => {
            entries.push(item.rangeStart);
            entries.push(item.rangeEnd);
        });

        entries = entries.sort((a, b) => a - b);

    }

    isBetween(x, min, max) {
        return x >= min && x <= max;
    }

}

export default new Services();