import services from './services';

class WaitingQueue {
    constructor() {
        this.registry = [];

        this.add('Jurgis Pavardenis', 1);
        this.add('Antanas Pavardenis', 1);
        this.add('Tadas Pavardenis', 1);
        this.add('Petras Pavardenis', 2);
        this.add('Albinas Pavardenis', 2);
    }

    add(name, serviceId) {
        let id = this.getAvailableServiceQueueId(serviceId);

        this.registry.push({
            id: id,
            name: name,
            arrivalTime: new Date(),
            serviceId: serviceId
        });
    }

    get(id) {
        let [result] = this.registry.filter(item => item.id === id);
        return result;
    }

    getAll() {
        return this.registry;
    }

    getServices() {
        return services.getAll();
    }

    getServiceQueues() {
        let services = this.getServices();
        return services.map(service => this.getServiceQueue(service.id));
    }

    getServiceQueue(serviceId) {
        return this.registry.filter(item => item.serviceId === serviceId)
    }

    getAvailableServiceQueueId(serviceId) {
        let range = services.getRange(serviceId);
        let queue = this.getServiceQueue(serviceId);
        let lastQueueRecord = queue.length ? queue[queue.length] : null;
        let i = lastQueueRecord ? lastQueueRecord.id : null;
        let isOccupied;
        do {
            if (!i || i === range.end) {
                i = range.start;
            } else {
                i = i + 1;
            }
            isOccupied = this.get(i) ? true : false;
        } while (isOccupied);

        return i;
    }
}

export default new WaitingQueue();