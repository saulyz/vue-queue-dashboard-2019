class ServedQueue {
    constructor() {
        this.registry = [];

        this.registry.push({
            id: 201,
            name: 'Jonas Jonaitis',
            arrivalTime: new Date(),
            serviceId: 1,
            serviceStartTime: new Date(),
            serviceEndTime: new Date(),
            servedById: 1
        });
    }
}

export default new ServedQueue();